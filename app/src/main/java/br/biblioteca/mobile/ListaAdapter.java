package br.biblioteca.mobile;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;

import java.io.File;
import java.util.List;

import br.biblioteca.mobile.api.Book;

public class ListaAdapter extends BaseAdapter {

    private Context context;
    private List<Book> lista;
    private ViewHolder holder;

    public ListaAdapter(Context context, List<Book> lista) {
        this.context = context;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Book getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return lista.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Book book = lista.get(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.linha, null);
            holder = new ViewHolder();
            holder.titulo = (TextView) convertView.findViewById(R.id.nome);
            holder.foto = (ImageView) convertView.findViewById(R.id.foto);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingBar);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.titulo.setText(book.getTitulo());
        holder.ratingBar.setRating(book.getAvaliacao().floatValue());

        BibliotecaUtil.setCacheImg(book, holder.foto, Constants.PATH_URL + "/" + book.getUrl(), context);
        return convertView;
    }

    static class ViewHolder {
        TextView titulo;
        ImageView foto;
        RatingBar ratingBar;
    }



}
