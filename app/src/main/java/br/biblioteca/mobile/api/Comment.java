package br.biblioteca.mobile.api;

public class Comment {

    private Long evaluation = 1l;

    private String comment;

    public Comment() {

    }

    public Comment(Long evaluation, String comment) {
        this.evaluation = evaluation;
        this.comment = comment;
    }

    public Long getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Long evaluation) {
        this.evaluation = evaluation;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return this.comment + " : " + this.evaluation;
    }
}
