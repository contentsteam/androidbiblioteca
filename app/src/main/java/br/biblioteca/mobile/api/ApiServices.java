package br.biblioteca.mobile.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiServices {

    @GET("/book/list")
    Call<List<Book>> findAllBooks(@Header("Content-Type") String content_type);

    @GET("/book/{id}")
    Call<Book> getBook(@Header("Content-Type") String content_type, @Path("id") Long id);

    @PUT("/book/{id}/comments")
    Call<Book> sendComments(@Header("Content-Type") String content_type, @Path("id") Long id, @Body Comment commnet);
}
