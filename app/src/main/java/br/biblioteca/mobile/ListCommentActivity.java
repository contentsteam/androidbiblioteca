package br.biblioteca.mobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

import br.biblioteca.mobile.api.ApiClient;
import br.biblioteca.mobile.api.ApiServices;
import br.biblioteca.mobile.api.Book;
import br.biblioteca.mobile.api.Comment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListCommentActivity extends AppCompatActivity {

    private List<Comment> comments = new ArrayList<>();
    private ListView lista;
    private TextView titulo;
    private Book book;
    private ImageView capa;
    private ApiServices apiServices;
    private ProgressDialog progress = null;
    private  Long idBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_comments);
        Toolbar myChildToolbar = (Toolbar) findViewById(R.id.toolbar_comments);
        myChildToolbar.setTitleTextColor(Color.WHITE);

        setSupportActionBar(myChildToolbar);
        android.support.v7.app.ActionBar ab = getSupportActionBar();

        ab.setDisplayHomeAsUpEnabled(true);
        titulo = findViewById(R.id.titulo);
        capa = findViewById(R.id.capa);
        lista = findViewById(R.id.comments);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        if(intent != null){
            idBook = intent.getLongExtra("id", 1l);
            if(idBook != null){
                getBook(idBook);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.comentar:
                Intent intent = new Intent(ListCommentActivity.this, CommentActivity.class);
                intent.putExtra("id", idBook);
                startActivity(intent);

                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getBook(Long id) {
        progress = ProgressDialog.show(ListCommentActivity.this,
                "Aguarde ...", "Recebendo informações da web", true, true);
        apiServices = ApiClient.getClient().create(ApiServices.class);

        Call<Book> call = apiServices.getBook ("application/json", id);
        call.enqueue(new Callback<Book>() {

            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {
                if (response.isSuccessful()) {
                    book =  response.body();

                    if(book != null){
                        comments = book.getComments();
                        titulo.setText(book.getTitulo());

                        BibliotecaUtil.setCacheImg(book, capa, Constants.PATH_URL + "/" + book.getUrl(), getApplicationContext());

                        ArrayAdapter<Comment> adapter =
                                new ArrayAdapter<Comment>(ListCommentActivity.this, android.R.layout.simple_list_item_1, comments);
                        lista.setAdapter(adapter);
                    }
                    progress.dismiss();

                } else {
                    progress.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Book> call, Throwable t) {
                progress.dismiss();
                t.printStackTrace();
            }
        });
    }
}
