package br.biblioteca.mobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import br.biblioteca.mobile.api.ApiClient;
import br.biblioteca.mobile.api.ApiServices;
import br.biblioteca.mobile.api.Book;
import br.biblioteca.mobile.api.Comment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentActivity extends AppCompatActivity {

    private Button cancelar;
    private Button enviar;
    private ApiServices apiServices;
    private ProgressDialog progress = null;
    private Long idBook;
    private EditText comentario;
    private RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        Toolbar myChildToolbar = (Toolbar) findViewById(R.id.toolbar_send_comments);
        myChildToolbar.setTitleTextColor(Color.WHITE);

        setSupportActionBar(myChildToolbar);
        myChildToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        comentario = findViewById(R.id.comentario);
        ratingBar = findViewById(R.id.ratingBar);
        ratingBar.setRating(0);

        cancelar = findViewById(R.id.cancelar);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        enviar = findViewById(R.id.enviar);
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(idBook != null && comentario.getText().toString().length() > 0 ){

                    Long rating = (long) ratingBar.getRating();

                    enviarComentario(idBook, new Comment(rating, comentario.getText().toString()));
                    finish();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        if(intent != null){
            idBook = intent.getLongExtra("id", 1l);
        }
    }

    private void enviarComentario(Long id, Comment comentario){
        progress = ProgressDialog.show(CommentActivity.this,
                "Aguarde ...", "Recebendo informações da web", true, true);
        apiServices = ApiClient.getClient().create(ApiServices.class);
        Call<Book> call = apiServices.sendComments ("application/json", id, comentario);
        call.enqueue(new Callback<Book>() {
            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {
                if (response.isSuccessful()) {
                    progress.dismiss();
                    finish();
                } else {
                    progress.dismiss();
                    finish();
                }
            }
            @Override
            public void onFailure(Call<Book> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }
}
